$(document).ready(function () {
  $.get('/comprejuntosku/16', function (data) {
    $('section.compre-junto-home.cj1-127V').html(data);

    $('section.compre-junto-home.cj1-127V table tr:nth-of-type(1)').hide();
    $('section.compre-junto-home.cj1-127V table tr:nth-of-type(2)').hide();
    
    $('section.compre-junto-home.cj1-127V .comprar-junto a#lnkComprar, .slider-intro #slick-slide01 a').click(function (e) {
      e.preventDefault();
      $('.question__actions').addClass('slide1-127V');
      var comprejunto2 = $('section.compre-junto-home.cj1-127V table tr:nth-of-type(3) p.comprar-junto a').attr('href');
      $('.question__actions.slide1-127V').html('<a href=' + comprejunto2 + '><span class="btn btn--width 220-volt">220 V</span></a>');
      $('body.home .popups #popup-comprar-junto').addClass('open');
    });
  });

  $.get('/comprejuntosku/21', function (data) {
    $('section.compre-junto-home.cj2-127V').html(data);

    $('section.compre-junto-home.cj2-127V table tr:nth-of-type(1)').hide();
    $('section.compre-junto-home.cj2-127V table tr:nth-of-type(2)').hide();

    var comprejunto2 = $('section.compre-junto-home.cj2-127V table tr:nth-of-type(3) p.comprar-junto a').attr('href');
    $('.slider-intro #slick-slide02 a').attr('href', comprejunto2);
  });

  $.get('/comprejuntosku/16', function (data) {
    $('section.cj3-220V').html(data);
    $('section.compre-junto-home.cj3-127V').html(data);

    $('section.compre-junto-home.cj3-127V table tr:nth-of-type(2)').hide();
    $('section.compre-junto-home.cj3-127V table tr:nth-of-type(3)').hide();

    var sem_voltB = $('section.compre-junto-home.cj3-127V table tr:nth-of-type(1) td.itemB a#lnkProdAtualFilho').text().split('127V')[0]
    $('section.compre-junto-home.cj3-127V table tr:nth-of-type(1) td.itemB a#lnkProdAtualFilho').html(sem_voltB);
    
    $('section.compre-junto-home.cj3-127V .comprar-junto a#lnkComprar, .slider-intro #slick-slide03 a').click(function (e) {
      e.preventDefault();
      $('body.home .popups #popup-comprar-junto').addClass('open');
      $('.question__actions').addClass('slide3-127V');
      var comprejunto = $('section.cj3-127V table tr:nth-of-type(2) p.comprar-junto a').attr('href');
      var comprejunto2 = $('section.cj3-220V table tr:nth-of-type(1) p.comprar-junto a').attr('href');
      $('.question__actions.slide3-127V').html('<a href=' + comprejunto2 + '><span class="btn 127-volt">127 V</span></a>')
      $('.question__actions.slide3-127V').append('<a href=' + comprejunto + '><span class="btn btn--width 220-volt">220 V</span></a>');
    });
  });


  $.get('/comprejuntosku/13', function (data) {
    $('section.cj4-220V').html(data);
  });

  $.get('/comprejuntosku/12', function (data) {
    $('section.compre-junto-home.cj4-127V').html(data);

    var sem_voltA = $('section.compre-junto-home.cj4-127V table tr:nth-of-type(1) td.itemA a#lnkProdAtualPai').text().split('127V')[0]
    $('section.compre-junto-home.cj4-127V table tr:nth-of-type(1) td.itemA a#lnkProdAtualPai').html(sem_voltA);
    
    $('section.compre-junto-home.cj4-127V .comprar-junto a#lnkComprar, .slider-intro #slick-slide04 a').click(function (e) {
      e.preventDefault();
      $('body.home .popups #popup-comprar-junto').addClass('open');
      $('.question__actions').addClass('slide4-127V');
      var comprejunto = $('section.compre-junto-home.cj4-127V table tr:nth-of-type(1) p.comprar-junto a').attr('href');
      $('.question__actions.slide4-127V').html('<a href=' + comprejunto + '><span class="btn 127-volt">127 V</span></a>')
      var comprejunto2 = $('section.cj4-220V table tr:nth-of-type(1) p.comprar-junto a').attr('href');
      $('.question__actions.slide4-127V').append('<a href=' + comprejunto2 + '><span class="btn btn--width 220-volt">220 V</span></a>');
    });
  });


  $.get('/comprejuntosku/20', function (data) {
    $('section.cj5-220V').html(data);
    $('section.compre-junto-home.cj5-127V').html(data);

    $('section.compre-junto-home.cj5-127V table tr:nth-of-type(2)').hide();

    var sem_voltB = $('section.compre-junto-home.cj5-127V table tr:nth-of-type(1) td.itemB a#lnkProdAtualFilho').text().split('127V')[0]
    $('section.compre-junto-home.cj5-127V table tr:nth-of-type(1) td.itemB a#lnkProdAtualFilho').html(sem_voltB);
    
    $('section.compre-junto-home.cj5-127V .comprar-junto a#lnkComprar, .slider-intro #slick-slide05 a').click(function (e) {
      e.preventDefault();
      $('body.home .popups #popup-comprar-junto').addClass('open');
      $('.question__actions').addClass('slide5-127V');
      var comprejunto = $('section.cj5-127V table tr:nth-of-type(2) p.comprar-junto a').attr('href');
      var comprejunto2 = $('section.cj5-220V table tr:nth-of-type(1) p.comprar-junto a').attr('href');
      $('.question__actions.slide5-127V').html('<a href=' + comprejunto2 + '><span class="btn 127-volt">127 V</span></a>')
      $('.question__actions.slide5-127V').append('<a href=' + comprejunto + '><span class="btn btn--width 220-volt">220 V</span></a>');
    });
  });

  $.get('/comprejuntosku/21', function (data) {
    $('section.cj6-220V').html(data);
    $('section.compre-junto-home.cj6-127V').html(data);

    $('section.compre-junto-home.cj6-127V table tr:nth-of-type(2)').hide();
    $('section.compre-junto-home.cj6-127V table tr:nth-of-type(3)').hide();

    var sem_voltB = $('section.compre-junto-home.cj6-127V table tr:nth-of-type(1) td.itemB a#lnkProdAtualFilho').text().split('127V')[0]
    $('section.compre-junto-home.cj6-127V table tr:nth-of-type(1) td.itemB a#lnkProdAtualFilho').html(sem_voltB);
    
    $('section.compre-junto-home.cj6-127V .comprar-junto a#lnkComprar, .slider-intro #slick-slide06 a').click(function (e) {
      e.preventDefault();
      $('body.home .popups #popup-comprar-junto').addClass('open');
      $('.question__actions').addClass('slide6-127V');
      var comprejunto = $('section.cj6-127V table tr:nth-of-type(1) p.comprar-junto a').attr('href');
      var comprejunto2 = $('section.cj6-220V table tr:nth-of-type(2) p.comprar-junto a').attr('href');
      $('.question__actions.slide6-127V').html('<a href=' + comprejunto + '><span class="btn 127-volt">127 V</span></a>')
      $('.question__actions.slide6-127V').append('<a href=' + comprejunto2 + '><span class="btn btn--width 220-volt">220 V</span></a>');
    });
  });

  
  $.get('/comprejuntosku/151', function (data) {
    $('section.cj7-220V-1').html(data);
  });

  $.get('/comprejuntosku/140', function (data) {
    $('section.cj7-220V-2').html(data);
    $('section.compre-junto-home.cj7-127V').html(data);

    $('section.compre-junto-home.cj7-127V table tr:nth-of-type(2)').hide();

    var sem_voltA = $('section.compre-junto-home.cj7-127V table tr:nth-of-type(1) td.itemA a#lnkProdAtualPai').text().split('127V')[0]
    $('section.compre-junto-home.cj7-127V table tr:nth-of-type(1) td.itemB a#lnkProdAtualPai').html(sem_voltA);

    var sem_voltB = $('section.compre-junto-home.cj7-127V table tr:nth-of-type(1) td.itemB a#lnkProdAtualFilho').text().split('127V')[0]
    $('section.compre-junto-home.cj7-127V table tr:nth-of-type(1) td.itemB a#lnkProdAtualFilho').html(sem_voltB);
    
    $('section.compre-junto-home.cj7-127V .comprar-junto a#lnkComprar, .slider-intro #slick-slide07 a').click(function (e) {
      e.preventDefault();
      $('body.home .popups #popup-comprar-junto').addClass('open');
      $('.question__actions').addClass('slide7-127V');
      var comprejunto = $('section.cj7-220V-1 table tr:nth-of-type(1) p.comprar-junto a').attr('href');
      var comprejunto2 = $('section.cj7-220V-2 table tr:nth-of-type(2) p.comprar-junto a').attr('href');
      $('.question__actions.slide7-127V').html('<a href=' + comprejunto + '><span class="btn 127-volt">127 V</span></a>')
      $('.question__actions.slide7-127V').append('<a href=' + comprejunto2 + '><span class="btn btn--width 220-volt">220 V</span></a>');
    });
  });
});