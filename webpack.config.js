const path = require('path')
const webpack = require('webpack')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const MinifyPlugin = require("babel-minify-webpack-plugin")

module.exports = {
  mode: 'development',
  entry: [
    './src/js/index.js',
    './src/scss/index.scss',
    'webpack-hot-middleware/client'
  ],
  devServer: {
    contentBase: './arquivos',
    hot: true
  },
  module: {
    rules: [
      { 
        test: /\.m?js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env']
          }
        }
      },
      {
        test: /\.scss$/,
        use: [
          'style-loader',
          'css-loader',
          {
            loader: 'sass-loader',
            options: {
              outputStyle: 'compressed'
            }
          },
          'postcss-loader'
        ]
      }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      title: 'Webpack test'
    }),
    new CleanWebpackPlugin(),
    new webpack.optimize.OccurrenceOrderPlugin(),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),
    new MiniCssExtractPlugin({
      filename: 'bundle.min.css'
    }),
    new MinifyPlugin({}, {
      sourceMap: true
    })
  ],
  output: {
    filename: 'bundle.min.js',
    path: path.resolve(__dirname, 'arquivos'),
    publicPath: '/arquivos'
  }
}
