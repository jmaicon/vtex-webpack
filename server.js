const pkg = require('./package')
const express = require('express')
const webpack = require('webpack')
const webpackDevMiddleware = require('webpack-dev-middleware')
const middlewares = require('./middlewares')
const httpPlease = require('connect-http-please')
const url = require('url')
const proxy = require('proxy-middleware')

const app = express()
const config = require('./webpack.config.js')
const compiler = webpack(config)

app.use(webpackDevMiddleware(compiler, {
  publicPath: config.output.publicPath
}))

app.use(require('webpack-hot-middleware')(compiler))

app.use(function (req, res, next) {
  req.headers['accept-encoding'] = 'identity'
  return next()
})

const accountName = process.env.VTEX_ACCOUNT || pkg.accountName || 'basedevmkp'
const site = process.env.VTEX_ACCOUNT || pkg.site || accountName
const environment = process.env.VTEX_ENV || pkg.env || 'vtexcommercestable'
const secureUrl = process.env.VTEX_SECURE_URL || pkg.secureUrl || true
const port = process.env.PORT || pkg.port || 80
const portalHost = `${site}.${environment}.com.br`
let localHost = `${site}.vtexlocal.com.br`
let imgProxyOptions = url.parse(`http://${site}.vteximg.com.br/arquivos`)
let portalProxyOptions = url.parse(`http://${portalHost}/`)

if (port !== 80) {
  localHost += `:${port}`
}

if (secureUrl) {
  imgProxyOptions = url.parse(`https://${site}.vteximg.com.br/arquivos`)
  portalProxyOptions = url.parse(`https://${portalHost}/`)
}

imgProxyOptions.route = '/arquivos'
portalProxyOptions.preserveHost = true
portalProxyOptions.cookieRewrite = `${site}.vtexlocal.com.br`

app.use((req, res, next) => middlewares.rewriteLocationHeader(req, res, next, portalHost, localHost))

app.use(function(req, res, next) {
  req.headers.host = portalHost
  return next()
})

app.use((req, res, next) => middlewares.replaceHtmlBody(req, res, next, environment, accountName, site, secureUrl, port))
app.use((err, req, res, next) => middlewares.errorHandler(err, req, res, next))
app.use(httpPlease(portalHost, false))
app.use(proxy(imgProxyOptions))
app.use(proxy(portalProxyOptions))

app.listen(port, function() {
  console.log(`App running on http://${localHost}\n`)
})
